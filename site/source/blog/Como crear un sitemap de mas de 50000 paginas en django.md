Title: Crear sitemaps de más de 50000 urls en Django
Date: 2023-04-21
Tags: django, seo
Slug: sitemaps-mas-de-50000-urls-en-django
Author: __alexander__
Summary: Descubre cómo crear sitemaps para sitios web en Django con gran cantidad de urls. Mejora la navegación y relevancia de tu sitio para SEO. Ejemplo de código incluido.

Un sitemap es un archivo XML que contiene una lista de URLs de un sitio web. Este archivo ayuda a los motores de
búsqueda a indexar y rastrear todas las páginas de un sitio web. Pero si tu sitio web tiene más de 50,000 URLs, es posible
que necesites crear múltiples sitemaps para ayudar a los motores de búsqueda a rastrear e indexar todas tus páginas.

Aquí te mostramos cómo puedes organizar tus sitemaps de más de 50,000 URLs para tu sitio web en Django.

Algunos pasos a seguir antes de paginar el sitemap, que nos pueden facilitar el trabajo son:

<br>
## 1. Divide tu sitio web en secciones:

Organiza tu sitio web en secciones lógicas. Por ejemplo, si fuera un website de noticias, podríamos tener:

- noticias de última hora
- noticias locales
- noticias deportivas
- etc.

Esto te ayudará a crear sitemaps secundarios más pequeños.

<br>
## 2. Crea un archivo sitemap.xml principal que agrupe a estas secciones ya definidas:

Agrega las URLs de los sitemaps secundarios que vas a crear. Este archivo será el punto de entrada para los motores de
búsqueda.

Antes hemos mostrado ejemplos de cómo realizar
esto: [Cómo implementar sitemaps en Django](https://alexanderae.com/django-implementar-sitemap.html)

~~~
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   <sitemap>
      <loc>http://www.ejemplo.com/noticias_locales.xml.gz</loc>
   </sitemap>
   <sitemap>
      <loc>http://www.ejemplo.com/noticias_deportivas.xml.gz</loc>
   </sitemap>
   <sitemap>
      <loc>http://www.ejemplo.com/noticias_de_ultima_hora.xml.gz</loc>
   </sitemap>
</sitemapindex>
~~~

<br>
## 3. Crea sitemaps secundarios:

Crea los sitemaps secundarios para cada sección de tu sitio web. Cada sitemap secundario debe contener hasta 50,000
URLs.

<br>
## 4. Comprime los sitemaps secundarios (recomendado, opcional):

Comprime los sitemaps secundarios utilizando *gzip* para reducir su tamaño y aumentar la velocidad de descarga.
Esto lo podemos realizar con la configuración del servidor web (Apache o Nginx).

<br>
## 5. Agrega una caché a los sitemaps (recomendado, opcional)
   Esto para no realizar el cálculo después de cada visita, ya que no es tan frecuente que se agreguen nuevas páginas
   a la web (evaluable según cada caso). 

Este punto también lo pueden realizar desde *apache* / *nginx* para mayor eficiencia.
Noten que *Django* también es capaz de realizar esta tarea.

<br>

---

## ¿Qué sucede si uno de mis sitemaps, aún así excede las 50000 urls?

Django cuenta con paginación automática para estos casos.
Las url's generadas serían del tipo:

- http://127.0.0.1:8001/books/sitemap.xml
- http://127.0.0.1:8001/books/sitemap.xml?p=2
- http://127.0.0.1:8001/books/sitemap.xml?p=3
- etc.

Hay que notar que django utiliza el parámetro GET "p" para identificar a cada página y que no crea un índice que
las agrupe, por lo que esta tarea quedaría para nosotros.

<br>
## Ejemplo de código

Aquí adjunto un proyecto en el que se puede revisar el comportamiento de un website con más de un millón de libros en su
sitemap: <a href="https://gitlab.com/__alexander__/django-books-demo" target="_blank">Django books demo</a>

<br>
## Referencias

- <a href="https://docs.djangoproject.com/en/dev/ref/contrib/sitemaps/">Sitemaps en Django (documentación oficial)</a>
